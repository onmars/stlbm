// *****************************************************************************
// STLBM SOFTWARE LIBRARY

// Copyright © 2020 University of Geneva
// Authors: Jonas Latt, Christophe Coreixas, Joël Beny
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

// Implementation of AA-pattern structure-of-array, for TRT, with aggressive loop unrolling.

#pragma once

#include "lbm_q27.h"
#include <vector>
#include <array>
#include <tuple>

namespace aa_soa_trt_unrolled {

struct LBM {
    using CellData = double;
    static size_t sizeOfLattice(size_t nelem) { return 27 * nelem; }

    CellData* lattice;
    CellType* flag;
    std::array<int, 3>* c;
    int* opp;
    double* t;
    double omega;
    Dim dim;
    LBModel model;
    bool periodic = false;

    auto i_to_xyz (int i) const {
        int iX = i / (dim.ny * dim.nz);
        int remainder = i % (dim.ny * dim.nz);
        int iY = remainder / dim.nz;
        int iZ = remainder % dim.nz;
        return std::make_tuple(iX, iY, iZ);
    };

    size_t xyz_to_i (int x, int y, int z) const {
        return z + dim.nz * (y + dim.ny * x);
    };

    double& f (int i, int k) {
        return lattice[k * dim.nelem + i];
    }

    auto iniLattice (double& f0) {
        auto i = &f0 - lattice;
        for (int k = 0; k < 27; ++k) {
            f(i, k) = t[k];
        }
    };

    auto iniLattice (double& f0, double rho, std::array<double, 3> const& u) {
        auto i = &f0 - lattice;
        double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);
        for (int k = 0; k < 27; ++k) {
            double ck_u = c[k][0] * u[0] + c[k][1] * u[1] + c[k][2] * u[2];
            f(i, k) = rho * t[k] * (1. + 3. * ck_u + 4.5 * ck_u * ck_u - usqr);
        }
    };

    auto macro (double const& f0) {
        auto i = &f0 - lattice;
        double X_M1 = f(i, 0) + f(i, 3) + f(i, 4) + f(i, 5) + f(i, 6) + f(i, 9) + f(i,10) + f(i,11) + f(i,12);
        double X_P1 = f(i,14) + f(i,17) + f(i,18) + f(i,19) + f(i,20) + f(i,23) + f(i,24) + f(i,25) + f(i,26);
        double X_0  = f(i, 1) + f(i, 2) + f(i, 7) + f(i, 8) + f(i,13) + f(i,15) + f(i,16) + f(i,21) + f(i,22);

        double Y_M1 = f(i, 1) + f(i, 3) + f(i, 7) + f(i, 8) + f(i, 9) + f(i,10) + f(i,18) + f(i,25) + f(i,26);
        double Y_P1 = f(i,15) + f(i,17) + f(i,21) + f(i,22) + f(i,23) + f(i,24) + f(i, 4) + f(i,11) + f(i,12);

        double Z_M1 = f(i, 2) + f(i, 5) + f(i, 7) + f(i, 9) + f(i,11) + f(i,20) + f(i,22) + f(i,24) + f(i,26);
        double Z_P1 = f(i,16) + f(i,19) + f(i,21) + f(i,23) + f(i,25) + f(i, 6) + f(i, 8) + f(i,10) + f(i,12);

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return make_pair(rho, u);
    }

    auto macropop (std::array<double, 27> const& pop) {
        double X_M1 = pop[ 0] + pop[ 3] + pop[ 4] + pop[ 5] + pop[ 6] + pop[ 9] + pop[10] + pop[11] + pop[12];
        double X_P1 = pop[14] + pop[17] + pop[18] + pop[19] + pop[20] + pop[23] + pop[24] + pop[25] + pop[26];
        double X_0  = pop[ 1] + pop[ 2] + pop[ 7] + pop[ 8] + pop[13] + pop[15] + pop[16] + pop[21] + pop[22];

        double Y_M1 = pop[ 1] + pop[ 3] + pop[ 7] + pop[ 8] + pop[ 9] + pop[10] + pop[18] + pop[25] + pop[26];
        double Y_P1 = pop[15] + pop[17] + pop[21] + pop[22] + pop[23] + pop[24] + pop[ 4] + pop[11] + pop[12];

        double Z_M1 = pop[ 2] + pop[ 5] + pop[ 7] + pop[ 9] + pop[11] + pop[20] + pop[22] + pop[24] + pop[26];
        double Z_P1 = pop[16] + pop[19] + pop[21] + pop[23] + pop[25] + pop[ 6] + pop[ 8] + pop[10] + pop[12];

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return make_pair(rho, u);
    };

};

struct Even : public LBM {

    // Optimizations are based on: 
    // 1 - Computation of opposite equilibrium populations using symmetries
    // 2 - Precomputation of ck_u terms
    auto collideTrtUnrolled(int i, double rho, std::array<double, 3> const& u, double usqr) {
        double cku1 = u[0] + u[1];
        double cku2 =-u[0] + u[1];
        double cku3 = u[0] + u[2];
        double cku4 =-u[0] + u[2];
        double cku5 = u[1] + u[2];
        double cku6 =-u[1] + u[2];
        double cku7 = u[0] + u[1] + u[2];
        double cku8 =-u[0] + u[1] + u[2];
        double cku9 = u[0] - u[1] + u[2];
        double cku0 = u[0] + u[1] - u[2];
        
        double s_plus = omega;
        double s_minus = 8. * (2. - s_plus) / (8. - s_plus);
        double s1 = 1 - 0.5*(s_plus+s_minus);
        double s2 = 0.5*(-s_plus+s_minus);
        double s3 = 1 - s1;
        
        std::array<double, 27> feqRM;

        feqRM[F000] = rho * t[F000] * (1.                                 - usqr);

        feqRM[FM00] = rho * t[FM00] * (1. - 3. * u[0] + 4.5 * u[0] * u[0] - usqr);
        feqRM[FP00] = rho * t[FP00] * (     6. * u[0] ) + feqRM[FM00];
        
        feqRM[F0M0] = rho * t[F0M0] * (1. - 3. * u[1] + 4.5 * u[1] * u[1] - usqr);
        feqRM[F0P0] = rho * t[F0P0] * (     6. * u[1] ) + feqRM[F0M0];
        
        feqRM[F00M] = rho * t[F00M] * (1. - 3. * u[2] + 4.5 * u[2] * u[2] - usqr);
        feqRM[F00P] = rho * t[F00P] * (     6. * u[2] ) + feqRM[F00M];

        feqRM[FMM0] = rho * t[FMM0] * (1. - 3. * cku1 + 4.5 * cku1 * cku1 - usqr);
        feqRM[FPP0] = rho * t[FPP0] * (     6. * cku1 ) + feqRM[FMM0];
        feqRM[FPM0] = rho * t[FPM0] * (1. - 3. * cku2 + 4.5 * cku2 * cku2 - usqr);
        feqRM[FMP0] = rho * t[FMP0] * (     6. * cku2 ) + feqRM[FPM0];

        feqRM[FM0M] = rho * t[FM0M] * (1. - 3. * cku3 + 4.5 * cku3 * cku3 - usqr);
        feqRM[FP0P] = rho * t[FP0P] * (     6. * cku3 ) + feqRM[FM0M];
        feqRM[FP0M] = rho * t[FP0M] * (1. - 3. * cku4 + 4.5 * cku4 * cku4 - usqr);
        feqRM[FM0P] = rho * t[FM0P] * (     6. * cku4 ) + feqRM[FP0M];

        feqRM[F0MM] = rho * t[F0MM] * (1. - 3. * cku5 + 4.5 * cku5 * cku5 - usqr);
        feqRM[F0PP] = rho * t[F0PP] * (     6. * cku5 ) + feqRM[F0MM];
        feqRM[F0PM] = rho * t[F0PM] * (1. - 3. * cku6 + 4.5 * cku6 * cku6 - usqr);
        feqRM[F0MP] = rho * t[F0MP] * (     6. * cku6 ) + feqRM[F0PM];

        feqRM[FMMM] = rho * t[FMMM] * (1. - 3. * cku7 + 4.5 * cku7 * cku7 - usqr);
        feqRM[FPPP] = rho * t[FPPP] * (     6. * cku7 ) + feqRM[FMMM];
        feqRM[FPMM] = rho * t[FPMM] * (1. - 3. * cku8 + 4.5 * cku8 * cku8 - usqr);
        feqRM[FMPP] = rho * t[FMPP] * (     6. * cku8 ) + feqRM[FPMM];
        feqRM[FMPM] = rho * t[FMPM] * (1. - 3. * cku9 + 4.5 * cku9 * cku9 - usqr);
        feqRM[FPMP] = rho * t[FPMP] * (     6. * cku9 ) + feqRM[FMPM];
        feqRM[FMMP] = rho * t[FMMP] * (1. - 3. * cku0 + 4.5 * cku0 * cku0 - usqr);
        feqRM[FPPM] = rho * t[FPPM] * (     6. * cku0 ) + feqRM[FMMP];

        // BGK Collision based on the second-order equilibrium
        std::array<double, 27> foutRM;

        foutRM[F000] = (1. - omega) * f(i,F000) + omega * feqRM[F000];

        foutRM[FP00] = f(i,FP00)*s1 + (f(i,FM00)-feqRM[FM00])*s2 + feqRM[FP00]*s3;
        foutRM[FM00] = f(i,FM00)*s1 + (f(i,FP00)-feqRM[FP00])*s2 + feqRM[FM00]*s3;

        foutRM[F0P0] = f(i,F0P0)*s1 + (f(i,F0M0)-feqRM[F0M0])*s2 + feqRM[F0P0]*s3;
        foutRM[F0M0] = f(i,F0M0)*s1 + (f(i,F0P0)-feqRM[F0P0])*s2 + feqRM[F0M0]*s3;

        foutRM[F00P] = f(i,F00P)*s1 + (f(i,F00M)-feqRM[F00M])*s2 + feqRM[F00P]*s3;
        foutRM[F00M] = f(i,F00M)*s1 + (f(i,F00P)-feqRM[F00P])*s2 + feqRM[F00M]*s3;

        foutRM[FPP0] = f(i,FPP0)*s1 + (f(i,FMM0)-feqRM[FMM0])*s2 + feqRM[FPP0]*s3;
        foutRM[FMP0] = f(i,FMP0)*s1 + (f(i,FPM0)-feqRM[FPM0])*s2 + feqRM[FMP0]*s3;
        foutRM[FPM0] = f(i,FPM0)*s1 + (f(i,FMP0)-feqRM[FMP0])*s2 + feqRM[FPM0]*s3;
        foutRM[FMM0] = f(i,FMM0)*s1 + (f(i,FPP0)-feqRM[FPP0])*s2 + feqRM[FMM0]*s3;

        foutRM[FP0P] = f(i,FP0P)*s1 + (f(i,FM0M)-feqRM[FM0M])*s2 + feqRM[FP0P]*s3;
        foutRM[FM0P] = f(i,FM0P)*s1 + (f(i,FP0M)-feqRM[FP0M])*s2 + feqRM[FM0P]*s3;
        foutRM[FP0M] = f(i,FP0M)*s1 + (f(i,FM0P)-feqRM[FM0P])*s2 + feqRM[FP0M]*s3;
        foutRM[FM0M] = f(i,FM0M)*s1 + (f(i,FP0P)-feqRM[FP0P])*s2 + feqRM[FM0M]*s3;

        foutRM[F0PP] = f(i,F0PP)*s1 + (f(i,F0MM)-feqRM[F0MM])*s2 + feqRM[F0PP]*s3;
        foutRM[F0MP] = f(i,F0MP)*s1 + (f(i,F0PM)-feqRM[F0PM])*s2 + feqRM[F0MP]*s3;
        foutRM[F0PM] = f(i,F0PM)*s1 + (f(i,F0MP)-feqRM[F0MP])*s2 + feqRM[F0PM]*s3;
        foutRM[F0MM] = f(i,F0MM)*s1 + (f(i,F0PP)-feqRM[F0PP])*s2 + feqRM[F0MM]*s3;

        foutRM[FPPP] = f(i,FPPP)*s1 + (f(i,FMMM)-feqRM[FMMM])*s2 + feqRM[FPPP]*s3;
        foutRM[FMPP] = f(i,FMPP)*s1 + (f(i,FPMM)-feqRM[FPMM])*s2 + feqRM[FMPP]*s3;
        foutRM[FPMP] = f(i,FPMP)*s1 + (f(i,FMPM)-feqRM[FMPM])*s2 + feqRM[FPMP]*s3;
        foutRM[FPPM] = f(i,FPPM)*s1 + (f(i,FMMP)-feqRM[FMMP])*s2 + feqRM[FPPM]*s3;
        foutRM[FMMP] = f(i,FMMP)*s1 + (f(i,FPPM)-feqRM[FPPM])*s2 + feqRM[FMMP]*s3;
        foutRM[FMPM] = f(i,FMPM)*s1 + (f(i,FPMP)-feqRM[FPMP])*s2 + feqRM[FMPM]*s3;
        foutRM[FPMM] = f(i,FPMM)*s1 + (f(i,FMPP)-feqRM[FMPP])*s2 + feqRM[FPMM]*s3;
        foutRM[FMMM] = f(i,FMMM)*s1 + (f(i,FPPP)-feqRM[FPPP])*s2 + feqRM[FMMM]*s3;

        f(i,F000) = foutRM[F000];

        f(i,FP00) = foutRM[FM00];
        f(i,FM00) = foutRM[FP00];

        f(i,F0P0) = foutRM[F0M0];
        f(i,F0M0) = foutRM[F0P0];

        f(i,F00P) = foutRM[F00M];
        f(i,F00M) = foutRM[F00P];

        f(i,FPP0) = foutRM[FMM0];
        f(i,FMP0) = foutRM[FPM0];
        f(i,FPM0) = foutRM[FMP0];
        f(i,FMM0) = foutRM[FPP0];

        f(i,FP0P) = foutRM[FM0M];
        f(i,FM0P) = foutRM[FP0M];
        f(i,FP0M) = foutRM[FM0P];
        f(i,FM0M) = foutRM[FP0P];

        f(i,F0PP) = foutRM[F0MM];
        f(i,F0MP) = foutRM[F0PM];
        f(i,F0PM) = foutRM[F0MP];
        f(i,F0MM) = foutRM[F0PP];

        f(i,FPPP) = foutRM[FMMM];
        f(i,FMPP) = foutRM[FPMM];
        f(i,FPMP) = foutRM[FMPM];
        f(i,FPPM) = foutRM[FMMP];
        f(i,FMMP) = foutRM[FPPM];
        f(i,FMPM) = foutRM[FPMP];
        f(i,FPMM) = foutRM[FMPP];
        f(i,FMMM) = foutRM[FPPP];
    }


    void iterateTrtUnrolled(double& f0) {
        int i = &f0 - lattice;
        if (flag[i] == CellType::bulk) {
            auto[rho, u] = macro(f0);
            double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);
            collideTrtUnrolled(i, rho, u, usqr);
        }
    }

    void operator() (double& f0) {
        iterateTrtUnrolled(f0);
    }
};

struct Odd : public LBM {

    // Optimizations are based on: 
    // 1 - Computation of opposite equilibrium populations using symmetries
    // 2 - Precomputation of ck_u terms
    auto collideTrtUnrolled(std::array<double, 27>& pop, double rho, std::array<double, 3> const& u, double usqr) {
        double cku1 = u[0] + u[1];
        double cku2 =-u[0] + u[1];
        double cku3 = u[0] + u[2];
        double cku4 =-u[0] + u[2];
        double cku5 = u[1] + u[2];
        double cku6 =-u[1] + u[2];
        double cku7 = u[0] + u[1] + u[2];
        double cku8 =-u[0] + u[1] + u[2];
        double cku9 = u[0] - u[1] + u[2];
        double cku0 = u[0] + u[1] - u[2];
        
        double s_plus = omega;
        double s_minus = 8. * (2. - s_plus) / (8. - s_plus);
        double s1 = 1 - 0.5*(s_plus+s_minus);
        double s2 = 0.5*(-s_plus+s_minus);
        double s3 = 1 - s1;
        
        std::array<double, 27> feqRM;

        feqRM[F000] = rho * t[F000] * (1.                                 - usqr);

        feqRM[FM00] = rho * t[FM00] * (1. - 3. * u[0] + 4.5 * u[0] * u[0] - usqr);
        feqRM[FP00] = rho * t[FP00] * (     6. * u[0] ) + feqRM[FM00];
        
        feqRM[F0M0] = rho * t[F0M0] * (1. - 3. * u[1] + 4.5 * u[1] * u[1] - usqr);
        feqRM[F0P0] = rho * t[F0P0] * (     6. * u[1] ) + feqRM[F0M0];
        
        feqRM[F00M] = rho * t[F00M] * (1. - 3. * u[2] + 4.5 * u[2] * u[2] - usqr);
        feqRM[F00P] = rho * t[F00P] * (     6. * u[2] ) + feqRM[F00M];

        feqRM[FMM0] = rho * t[FMM0] * (1. - 3. * cku1 + 4.5 * cku1 * cku1 - usqr);
        feqRM[FPP0] = rho * t[FPP0] * (     6. * cku1 ) + feqRM[FMM0];
        feqRM[FPM0] = rho * t[FPM0] * (1. - 3. * cku2 + 4.5 * cku2 * cku2 - usqr);
        feqRM[FMP0] = rho * t[FMP0] * (     6. * cku2 ) + feqRM[FPM0];

        feqRM[FM0M] = rho * t[FM0M] * (1. - 3. * cku3 + 4.5 * cku3 * cku3 - usqr);
        feqRM[FP0P] = rho * t[FP0P] * (     6. * cku3 ) + feqRM[FM0M];
        feqRM[FP0M] = rho * t[FP0M] * (1. - 3. * cku4 + 4.5 * cku4 * cku4 - usqr);
        feqRM[FM0P] = rho * t[FM0P] * (     6. * cku4 ) + feqRM[FP0M];

        feqRM[F0MM] = rho * t[F0MM] * (1. - 3. * cku5 + 4.5 * cku5 * cku5 - usqr);
        feqRM[F0PP] = rho * t[F0PP] * (     6. * cku5 ) + feqRM[F0MM];
        feqRM[F0PM] = rho * t[F0PM] * (1. - 3. * cku6 + 4.5 * cku6 * cku6 - usqr);
        feqRM[F0MP] = rho * t[F0MP] * (     6. * cku6 ) + feqRM[F0PM];

        feqRM[FMMM] = rho * t[FMMM] * (1. - 3. * cku7 + 4.5 * cku7 * cku7 - usqr);
        feqRM[FPPP] = rho * t[FPPP] * (     6. * cku7 ) + feqRM[FMMM];
        feqRM[FPMM] = rho * t[FPMM] * (1. - 3. * cku8 + 4.5 * cku8 * cku8 - usqr);
        feqRM[FMPP] = rho * t[FMPP] * (     6. * cku8 ) + feqRM[FPMM];
        feqRM[FMPM] = rho * t[FMPM] * (1. - 3. * cku9 + 4.5 * cku9 * cku9 - usqr);
        feqRM[FPMP] = rho * t[FPMP] * (     6. * cku9 ) + feqRM[FMPM];
        feqRM[FMMP] = rho * t[FMMP] * (1. - 3. * cku0 + 4.5 * cku0 * cku0 - usqr);
        feqRM[FPPM] = rho * t[FPPM] * (     6. * cku0 ) + feqRM[FMMP];

        // TRT collision based on the second-order equilibrium
        std::array<double, 27> foutRM;

        foutRM[F000] = (1. - omega) * pop[F000] + omega * feqRM[F000];

        foutRM[FP00] = pop[FP00]*s1 + (pop[FM00]-feqRM[FM00])*s2 + feqRM[FP00]*s3;
        foutRM[FM00] = pop[FM00]*s1 + (pop[FP00]-feqRM[FP00])*s2 + feqRM[FM00]*s3;

        foutRM[F0P0] = pop[F0P0]*s1 + (pop[F0M0]-feqRM[F0M0])*s2 + feqRM[F0P0]*s3;
        foutRM[F0M0] = pop[F0M0]*s1 + (pop[F0P0]-feqRM[F0P0])*s2 + feqRM[F0M0]*s3;

        foutRM[F00P] = pop[F00P]*s1 + (pop[F00M]-feqRM[F00M])*s2 + feqRM[F00P]*s3;
        foutRM[F00M] = pop[F00M]*s1 + (pop[F00P]-feqRM[F00P])*s2 + feqRM[F00M]*s3;

        foutRM[FPP0] = pop[FPP0]*s1 + (pop[FMM0]-feqRM[FMM0])*s2 + feqRM[FPP0]*s3;
        foutRM[FMP0] = pop[FMP0]*s1 + (pop[FPM0]-feqRM[FPM0])*s2 + feqRM[FMP0]*s3;
        foutRM[FPM0] = pop[FPM0]*s1 + (pop[FMP0]-feqRM[FMP0])*s2 + feqRM[FPM0]*s3;
        foutRM[FMM0] = pop[FMM0]*s1 + (pop[FPP0]-feqRM[FPP0])*s2 + feqRM[FMM0]*s3;

        foutRM[FP0P] = pop[FP0P]*s1 + (pop[FM0M]-feqRM[FM0M])*s2 + feqRM[FP0P]*s3;
        foutRM[FM0P] = pop[FM0P]*s1 + (pop[FP0M]-feqRM[FP0M])*s2 + feqRM[FM0P]*s3;
        foutRM[FP0M] = pop[FP0M]*s1 + (pop[FM0P]-feqRM[FM0P])*s2 + feqRM[FP0M]*s3;
        foutRM[FM0M] = pop[FM0M]*s1 + (pop[FP0P]-feqRM[FP0P])*s2 + feqRM[FM0M]*s3;

        foutRM[F0PP] = pop[F0PP]*s1 + (pop[F0MM]-feqRM[F0MM])*s2 + feqRM[F0PP]*s3;
        foutRM[F0MP] = pop[F0MP]*s1 + (pop[F0PM]-feqRM[F0PM])*s2 + feqRM[F0MP]*s3;
        foutRM[F0PM] = pop[F0PM]*s1 + (pop[F0MP]-feqRM[F0MP])*s2 + feqRM[F0PM]*s3;
        foutRM[F0MM] = pop[F0MM]*s1 + (pop[F0PP]-feqRM[F0PP])*s2 + feqRM[F0MM]*s3;

        foutRM[FPPP] = pop[FPPP]*s1 + (pop[FMMM]-feqRM[FMMM])*s2 + feqRM[FPPP]*s3;
        foutRM[FMPP] = pop[FMPP]*s1 + (pop[FPMM]-feqRM[FPMM])*s2 + feqRM[FMPP]*s3;
        foutRM[FPMP] = pop[FPMP]*s1 + (pop[FMPM]-feqRM[FMPM])*s2 + feqRM[FPMP]*s3;
        foutRM[FPPM] = pop[FPPM]*s1 + (pop[FMMP]-feqRM[FMMP])*s2 + feqRM[FPPM]*s3;
        foutRM[FMMP] = pop[FMMP]*s1 + (pop[FPPM]-feqRM[FPPM])*s2 + feqRM[FMMP]*s3;
        foutRM[FMPM] = pop[FMPM]*s1 + (pop[FPMP]-feqRM[FPMP])*s2 + feqRM[FMPM]*s3;
        foutRM[FPMM] = pop[FPMM]*s1 + (pop[FMPP]-feqRM[FMPP])*s2 + feqRM[FPMM]*s3;
        foutRM[FMMM] = pop[FMMM]*s1 + (pop[FPPP]-feqRM[FPPP])*s2 + feqRM[FMMM]*s3;

        pop[F000] = foutRM[F000];

        pop[FP00] = foutRM[FP00];
        pop[FM00] = foutRM[FM00];

        pop[F0P0] = foutRM[F0P0];
        pop[F0M0] = foutRM[F0M0];

        pop[F00P] = foutRM[F00P];
        pop[F00M] = foutRM[F00M];

        pop[FPP0] = foutRM[FPP0];
        pop[FMP0] = foutRM[FMP0];
        pop[FPM0] = foutRM[FPM0];
        pop[FMM0] = foutRM[FMM0];

        pop[FP0P] = foutRM[FP0P];
        pop[FM0P] = foutRM[FM0P];
        pop[FP0M] = foutRM[FP0M];
        pop[FM0M] = foutRM[FM0M];

        pop[F0PP] = foutRM[F0PP];
        pop[F0MP] = foutRM[F0MP];
        pop[F0PM] = foutRM[F0PM];
        pop[F0MM] = foutRM[F0MM];

        pop[FPPP] = foutRM[FPPP];
        pop[FMPP] = foutRM[FMPP];
        pop[FPMP] = foutRM[FPMP];
        pop[FPPM] = foutRM[FPPM];
        pop[FMMP] = foutRM[FMMP];
        pop[FMPM] = foutRM[FMPM];
        pop[FPMM] = foutRM[FPMM];
        pop[FMMM] = foutRM[FMMM];
    }


    void iterateTrtUnrolled(double& f0) {
        int i = &f0 - lattice;
        auto[iX, iY, iZ] = i_to_xyz(i);
        CellType cellType = flag[i];

        if (cellType == CellType::bulk) {
            std::array<double, 27> pop;
            if (periodic) {
                for (int k = 0; k < 27; ++k) {
                    int XX = (iX - c[k][0] + dim.nx) % dim.nx;
                    int YY = (iY - c[k][1] + dim.ny) % dim.ny;
                    int ZZ = (iZ - c[k][2] + dim.nz) % dim.nz;
                    size_t nb = xyz_to_i(XX, YY, ZZ);
                    CellType nbCellType = flag[nb];
                    if (nbCellType == CellType::bounce_back) {
                        pop[k] = f(i, k) + f(nb, opp[k]);
                    }
                    else {
                        pop[k] = f(nb, opp[k]);
                    }
                }
            }
            else {
                for (int k = 0; k < 27; ++k) {
                    int XX = iX - c[k][0];
                    int YY = iY - c[k][1];
                    int ZZ = iZ - c[k][2];
                    size_t nb = xyz_to_i(XX, YY, ZZ);
                    CellType nbCellType = flag[nb];
                    if (nbCellType == CellType::bounce_back) {
                        pop[k] = f(i, k) + f(nb, opp[k]);
                    }
                    else {
                        pop[k] = f(nb, opp[k]);
                    }
                }
            }

            auto[rho, u] = macropop(pop);
            double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);

            collideTrtUnrolled(pop, rho, u, usqr);

            if (periodic) {
                for (int k = 0; k < 27; ++k) {
                    int XX = (iX + c[k][0] + dim.nx) % dim.nx;
                    int YY = (iY + c[k][1] + dim.ny) % dim.ny;
                    int ZZ = (iZ + c[k][2] + dim.nz) % dim.nz;
                    size_t nb = xyz_to_i(XX, YY, ZZ);
                    CellType nbCellType = flag[nb];
                    if (nbCellType == CellType::bounce_back) {
                        f(i, opp[k]) = pop[k] + f(nb, k);
                    }
                    else {
                        f(nb, k) = pop[k];
                    }
                }
            }
            else {
                for (int k = 0; k < 27; ++k) {
                    int XX = iX + c[k][0];
                    int YY = iY + c[k][1];
                    int ZZ = iZ + c[k][2];
                    size_t nb = xyz_to_i(XX, YY, ZZ);
                    CellType nbCellType = flag[nb];
                    if (nbCellType == CellType::bounce_back) {
                        f(i, opp[k]) = pop[k] + f(nb, k);
                    }
                    else {
                        f(nb, k) = pop[k];
                    }
                }
            }
        }
    }

    void operator() (double& f0) {
        iterateTrtUnrolled(f0);
    }
};

} // namespace aa_soa_trt_unrolled
