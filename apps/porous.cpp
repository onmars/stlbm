// *****************************************************************************
// STLBM SOFTWARE LIBRARY

// Copyright © 2020 University of Geneva
// Authors: Jonas Latt, Christophe Coreixas, Joël Beny
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

// The file porous.cpp implements a pore-level flow in a porous media.
// To run it, you must place a configuration file names "config" in the current
// directory. An example configuration file should be available in the source
// directory of porous.cpp.

#include "lbm.h"

#include "twopop_aos.h"
#include "twopop_aos_trt_unrolled.h"
#include "twopop_soa.h"
#include "twopop_soa_trt_unrolled.h"

#include "swap_aos.h"
#include "swap_aos_trt_unrolled.h"
#include "swap_soa.h"
#include "swap_soa_trt_unrolled.h"

#include "aa_aos.h"
#include "aa_aos_trt_unrolled.h"
#include "aa_soa.h"
#include "aa_soa_trt_unrolled.h"

#include "utility.h"
#include <string>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <execution>
#include <chrono>
#include <cassert>

using namespace std;
using namespace std::chrono;

int out_freq = 0;       // Non-benchmark mode: Frequency in LU of output message (use 0 for no messages)
int vtk_freq = 0;       // Non-benchmark mode: Frequency in LU of VTK data output (use 0 for no VTK)
int data_freq = 0;      // Non-benchmark mode: Frequency in LU of full data dump (use 0 for no data dump)
int bench_ini_iter = 0; // Benchmark mode: Number of warmup iterations
int bench_max_iter = 0; // Benchmark mode: Total number of iterations

double nuPhys = 0.;
double tau = 0.;
double ulb = 0.;
double dx = 0.;

template<class LBM>
void iniPorousMedium(LBM& lbm, string rawdata_fname, Dim const& dimData,
                     Dim const& dimExtract, int buffer, double ulb)
{
    assert( dimExtract.nx <= dimData.nx );
    assert( dimExtract.ny <= dimData.ny );
    assert( dimExtract.nz <= dimData.nz );

    std::vector<char> geometry((size_t)dimData.nx * (size_t)dimData.ny * (size_t)dimData.nz);
    ifstream ifile(rawdata_fname.c_str(), ios::binary);
    if (!ifile) {
        throw invalid_argument(string("File ") + rawdata_fname + string(" could not be opened."));
    }

    if (!ifile.read(&geometry[0], geometry.size())) {
        throw invalid_argument(string("Error reading data from file ") + rawdata_fname);
    }

    std::fill(lbm.flag, lbm.flag + lbm.dim.nelem, CellType::bulk);

    const int FLUID_TAG = 0;

    size_t iGeometry = 0;
    for (int iX = 0; iX < dimExtract.nx; ++iX) {
        for (int iY = 0; iY < dimData.ny; ++iY) {
            for (int iZ = 0; iZ < dimData.nz; ++iZ) {
                int porousFlag = (int) geometry[iGeometry++];
                if (iY < dimExtract.ny && iZ < dimExtract.nz) {
                    size_t i = lbm.xyz_to_i(iX + buffer, iY, iZ);
                    if (porousFlag == FLUID_TAG) {
                        lbm.flag[i] = CellType::bulk;
                    }
                    else {
                        lbm.flag[i] = CellType::bounce_back;
                        for (int k = 0; k < 19; ++k) {
                            lbm.f(i, k) = 0.;
                        }
                    }
                }
            }
        }
    }

    for (int iY = 0; iY < dimExtract.ny; ++iY) {
        for (int iZ = 0; iZ < dimExtract.nz; ++iZ) {
            size_t i0 = lbm.xyz_to_i(0, iY, iZ);
            size_t i1 = lbm.xyz_to_i(dimExtract.nx + 2 * buffer - 1, iY, iZ);
            lbm.flag[i0] = CellType::bounce_back;
            lbm.flag[i1] = CellType::bounce_back;
            for (int k = 0; k < 19; ++k) {
                lbm.f(i0, k) = - 6. * lbm.t[k] * ulb * lbm.c[k][0];
                lbm.f(i1, k) = - 6. * lbm.t[k] * ulb * lbm.c[k][0];
            }
        }
    }
}

auto lbParameters(double nuPhys, double tau, double dx) {
    double nu_lb = 1./3. * (tau - 0.5);
    double dt = nu_lb / nuPhys * dx * dx;
    return make_tuple(nu_lb, dt);
}

// Print the simulation parameters to the terminal.
void printParameters(bool benchmark, double omega, double ulb, double dx, double dt, double nu_lb, Dim dim, int max_iter) {
    double Re = (double)dim.ny * ulb / nu_lb;
    if (benchmark) {
        cout << "Flow through porous media, benchmark mode" << endl;
    }
    else {
        cout << "Flow through porous media, production mode" << endl;
    }
    cout << "Dimension (LU) = {" << dim.nx << ", " << dim.ny << ", " << dim.nz << "}" << endl;
    cout << "Re (wrt ny) = " << Re << endl;
    cout << "tau = " << 1. / omega << endl;
    cout << "ulb = " << ulb << endl;
    cout << "dx = " << dx << endl;
    cout << "dt = " << dt << endl;
    if (benchmark) {
        cout << "Now running " << bench_ini_iter << " warm-up iterations." << endl;
    }
    else {
        cout << "prod_max_iter = " << max_iter << endl;
    }
}

// Return a new clock for the current time, for benchmarking.
auto restartClock() {
    return make_pair(high_resolution_clock::now(), 0);
}

// Compute the time elapsed since a starting point, and the corresponding
// performance of the code in Mega Lattice site updates per second (MLups).
template<class TimePoint>
double printMlups(TimePoint start, int clock_iter, size_t nelem) {
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    double mlups = static_cast<double>(nelem * clock_iter) / duration.count();

    cout << "Benchmark result: " << setprecision(4) << mlups << " MLUPS" << endl;
    cout << endl;
    return mlups;
}

// Run a regression test for a specific pre-recorded value of the average energy.
void runRegression(double permeability, int time_iter) {
    double reference_permeability = 7.10362325943e-11;
    cout << "Regression test at iteration " << time_iter << ": Permeability = "
         << setprecision(12) << permeability;

    if (std::fabs(permeability - reference_permeability) < 1.e-11) {
        cout << ": OK" << endl;
    }
    else {
        cout << ": FAILED" << endl;
        cout << "Expected the value " << reference_permeability << endl;
        throw runtime_error("Regression test failed.");
    }
}

template<class LBM>
double runPorousTwoPop(bool benchmark, int prod_max_iter, LBModel model, string rawdata_fname,
                       Dim dimData, Dim dimExtract, int buffer)
{
    // CellData is either a double (structure-of-array) or an array<double, 19> (array-of-structure).
    using CellData = typename LBM::CellData; 

    Dim dim { dimExtract.nx + 2 * buffer, dimExtract.ny, dimExtract.nz };

    auto[nu_lb, dt] = lbParameters(nuPhys, tau, dx);
    double omega = 1. / tau;
    printParameters(benchmark, omega, ulb, dx, dt, nu_lb, dim, prod_max_iter);

    vector<CellData> lattice_vect(LBM::sizeOfLattice(dim.nelem));
    CellData* lattice = &lattice_vect[0];

    // The "vector" is used as a convenient way to allocate the flag array on the heap.
    vector<CellType> flag_vect(dim.nelem);
    CellType* flag = &flag_vect[0];

    // The "vector" is used as a convenient way to allocate the integer value "parity"
    // on the heap. That's a necessity when the code is used on GPUs, because the
    // parity (which is 0/1, depending on whether the current time iteration is even/odd)
    // is modified on the host, and accessed on the device. The parity flag is used
    // to decide in which of the two population arrays the pre-collision LB variables
    // are located.
    vector<int> parity_vect {0};
    int* parity = &parity_vect[0];

    // The lattice constants (discrete velocities, opposite indices, weghts) are mostly
    // used in the not-unrolled versions of the code. They must be allocated on the
    // heap so they can be shared with the device in case of a GPU execution.
    auto[c, opp, t] = d3q19_constants();
    // Instantiate the function object for the for_each call.
    bool periodic = true;
    LBM lbm{lattice, flag, parity, &c[0], &opp[0], &t[0], omega, dim, model, periodic};

    // Initialize the populations.
    for_each(lattice, lattice + dim.nelem, [&lbm](CellData& f0) { lbm.iniLattice(f0, 1., {ulb, 0., 0.}); });

    // Set up the geometry of the porous medium.
    iniPorousMedium(lbm, rawdata_fname, dimData, dimExtract, buffer, ulb);
    // Reset the clock, to be used when a benchmark simulation is executed.
    auto[start, clock_iter] = restartClock();

    ofstream permfile("permeability.dat");
    // Storage space for the time-averaged profiles, if such profiles are computed.
    int max_time_iter = benchmark ? bench_max_iter : prod_max_iter;
    for (int time_iter = 0; time_iter < max_time_iter; ++time_iter) {
        if (benchmark && time_iter == bench_ini_iter)  {
            cout << "Now running " << bench_max_iter - bench_ini_iter
                 << " benchmark iterations." << endl;
            tie(start, clock_iter) = restartClock();
        }
        // The regression case has been registered at a resolution of ny=100 after 150 iterations.
        // This test should be executed at every code change.
        if (dimExtract.ny == 50 && time_iter == 150) {
            runRegression(computePermeability(lbm, buffer, ulb, nu_lb, dx), time_iter);
        }
        // If this option is chosen in the config file, save the full domain in VTK files periodically.
        if (!benchmark && vtk_freq != 0 && time_iter % vtk_freq == 0 && time_iter > 0) {
            saveVtkFields(lbm, time_iter);
            tie(start, clock_iter) = restartClock();
        }
        // If this option is chosen in the config file, save the raw populations in text files periodically.
        if (!benchmark && data_freq != 0 && time_iter % data_freq == 0 && time_iter > 0) {
            saveFields(lbm);
            tie(start, clock_iter) = restartClock();
        }
        // If this option is chosen in the config file, output some statistics and write the center-line
        // velocity profiles to a text file.
        if (!benchmark && out_freq != 0 && time_iter % out_freq == 0 && time_iter > 0) {
            double maxVel = computeMaxVel(lbm);
            double permeability = computePermeability(lbm, buffer, ulb, nu_lb, dx);
            cout << "Permeability: " << setprecision(8) << permeability << endl;
            cout << "Maximum velocity (LU): " << setprecision(8) << maxVel << endl;
            permfile << setw(10) << time_iter * dt << setw(16) << setprecision(8) << permeability << endl;
            if (clock_iter > 0) {
                printMlups(start, clock_iter, dim.nelem);
            }
            else {
                cout << endl;
            }
        }

        // With the double-population scheme, collision and streaming are fused, and are executed in the
        // following loop.
        for_each(execution::par_unseq, lattice, lattice + dim.nelem, lbm);
        // After a collision-streaming cycle, swap the parity for the next iteration.
        *parity = 1 - *parity;
        ++clock_iter;
    }

    if (benchmark) {
        return printMlups(start, clock_iter, dim.nelem);
    }
    else {
        return 0.;
    }
}

template<class Collide, class Stream>
double runPorousSwap(bool benchmark, int prod_max_iter, LBModel model, string rawdata_fname,
                     Dim dimData, Dim dimExtract, int buffer)
{
    // CellData is either a double (structure-of-array) or an array<double, 19> (array-of-structure).
    using CellData = typename Collide::CellData;

    Dim dim { dimExtract.nx + 2 * buffer, dimExtract.ny, dimExtract.nz };

    auto[nu_lb, dt] = lbParameters(nuPhys, tau, dx);
    double omega = 1. / tau;
    printParameters(benchmark, omega, ulb, dx, dt, nu_lb, dim, prod_max_iter);

    vector<CellData> lattice_vect(Collide::sizeOfLattice(dim.nelem));
    CellData* lattice = &lattice_vect[0];

    // We use the "vector" as a convenient way to allocate the flag array on the heap.
    vector<CellType> flag_vect(dim.nelem);
    CellType* flag = &flag_vect[0];

    // The lattice constants (discrete velocities, opposite indices, weghts) are mostly
    // used in the not-unrolled versions of the code. They must be allocated on the
    // heap so they can be shared with the device in case of a GPU execution.
    auto[c, opp, t] = d3q19_constants();
    // Instantiate two function objects for the respective two for_each calls, as collision
    // and streaming are not fused with the swap scheme.
    bool periodic = true;
    Collide collide{{lattice, flag, &c[0], &opp[0], &t[0], omega, dim, model, periodic}};
    Stream stream{{lattice, flag, &c[0], &opp[0], &t[0], omega, dim, model, periodic}};

    // Initialize the populations.
    for_each(lattice, lattice + dim.nelem, [&collide](CellData& f0) { collide.iniLattice(f0, 1., {ulb, 0., 0.}); });

    // Set up the geometry of the porous medium.
    iniPorousMedium(collide, rawdata_fname, dimData, dimExtract, buffer, ulb);
    // Reset the clock, to be used when a benchmark simulation is executed.
    auto[start, clock_iter] = restartClock();

    ofstream permfile("permeability.dat");
    // Storage space for the time-averaged profiles, if such profiles are computed.
    int max_time_iter = benchmark ? bench_max_iter : prod_max_iter;
    for (int time_iter = 0; time_iter < max_time_iter; ++time_iter) {
        if (benchmark && time_iter == bench_ini_iter)  {
            cout << "Now running " << bench_max_iter - bench_ini_iter
                 << " benchmark iterations." << endl;
            tie(start, clock_iter) = restartClock();
        }
        // The regression case has been registered at a resolution of ny=100 after 150 iterations.
        // This test should be executed at every code change.
        if (dimExtract.ny == 50 && time_iter == 150) {
            runRegression(computePermeability(collide, buffer, ulb, nu_lb, dx), time_iter);
        }
        // If this option is chosen in the config file, save the full domain in VTK files periodically.
        if (!benchmark && vtk_freq != 0 && time_iter % vtk_freq == 0 && time_iter > 0) {
            saveVtkFields(collide, time_iter);
            tie(start, clock_iter) = restartClock();
        }
        // If this option is chosen in the config file, save the raw populations in text files periodically.
        if (!benchmark && data_freq != 0 && time_iter % data_freq == 0 && time_iter > 0) {
            saveFields(collide);
            tie(start, clock_iter) = restartClock();
        }
        // If this option is chosen in the config file, output some statistics and write the center-line
        // velocity profiles to a text file.
        if (!benchmark && out_freq != 0 && time_iter % out_freq == 0 && time_iter > 0) {
            double maxVel = computeMaxVel(collide);
            double permeability = computePermeability(collide, buffer, ulb, nu_lb, dx);
            cout << "Permeability: " << setprecision(8) << permeability << endl;
            cout << "Maximum velocity (LU): " << setprecision(8) << maxVel << endl;
            permfile << setw(10) << time_iter * dt << setw(16) << setprecision(8) << permeability << endl;
            if (clock_iter > 0) {
                printMlups(start, clock_iter, dim.nelem);
            }
        }

        // Collision and streaming are not fused. Execute a collision step ...
        for_each(execution::par_unseq, lattice, lattice + dim.nelem, collide);
        // ... and thereafter a streaming step. The necessity for two for_each calls is motivated
        // by thread saftey (all threads are synchronized between subsequent calls to for_each).
        for_each(execution::par_unseq, lattice, lattice + dim.nelem, stream);
        ++clock_iter;
    }

    if (benchmark) {
        return printMlups(start, clock_iter, dim.nelem);
    }
    else {
        return 0.;
    }
}

template<class Even, class Odd>
double runPorousAA(bool benchmark, int prod_max_iter, LBModel model, string rawdata_fname,
                   Dim dimData, Dim dimExtract, int buffer)
{
    // CellData is either a double (structure-of-array) or an array<double, 19> (array-of-structure).
    using CellData = typename Even::CellData;

    Dim dim { dimExtract.nx + 2 * buffer, dimExtract.ny, dimExtract.nz };

    auto[nu_lb, dt] = lbParameters(nuPhys, tau, dx);
    double omega = 1. / tau;
    printParameters(benchmark, omega, ulb, dx, dt, nu_lb, dim, prod_max_iter);

    // We use the "vector" as a convenient way to allocate the flag array on the heap.
    vector<CellData> lattice_vect(Even::sizeOfLattice(dim.nelem));
    CellData* lattice = &lattice_vect[0];

    vector<CellType> flag_vect(dim.nelem);
    CellType* flag = &flag_vect[0];

    // The lattice constants (discrete velocities, opposite indices, weghts) are mostly
    // used in the not-unrolled versions of the code. They must be allocated on the
    // heap so they can be shared with the device in case of a GPU execution.
    auto[c, opp, t] = d3q19_constants();
    // Instantiate two function objects for the for_each calls at even and at odd time
    // steps respectively. Note that collision and streaming are fused: only one of the
    // two is function objects is used at every time step.
    bool periodic = true;
    Even even{{lattice, flag, &c[0], &opp[0], &t[0], omega, dim, model, periodic}};
    Odd odd{{lattice, flag, &c[0], &opp[0], &t[0], omega, dim, model, periodic}};

    // Initialize the populations.
    for_each(lattice, lattice + dim.nelem, [&even](CellData& f0) { even.iniLattice(f0, 1., {ulb, 0., 0.}); });

    // Set up the geometry of the porous medium.
    iniPorousMedium(even, rawdata_fname, dimData, dimExtract, buffer, ulb);
    // Reset the clock, to be used when a benchmark simulation is executed.
    auto[start, clock_iter] = restartClock();

    ofstream permfile("permeability.dat");
    // Storage space for the time-averaged profiles, if such profiles are computed.
    int max_time_iter = benchmark ? bench_max_iter : prod_max_iter;
    for (int time_iter = 0; time_iter < max_time_iter; ++time_iter) {
        if (benchmark && time_iter == bench_ini_iter)  {
            cout << "Now running " << bench_max_iter - bench_ini_iter
                 << " benchmark iterations." << endl;
            tie(start, clock_iter) = restartClock();
        }
        // The regression case has been registered at a resolution of ny=100 after 150 iterations.
        // This test should be executed at every code change.
        if (dimExtract.ny == 50 && time_iter == 150) {
            runRegression(computePermeability(even, buffer, ulb, nu_lb, dx), time_iter);
        }
        // If this option is chosen in the config file, save the full domain in VTK files periodically.
        if (!benchmark && vtk_freq != 0 && time_iter % vtk_freq == 0 && time_iter > 0) {
            saveVtkFields(even, time_iter);
            tie(start, clock_iter) = restartClock();
        }
        // If this option is chosen in the config file, save the raw populations in text files periodically.
        if (!benchmark && data_freq != 0 && time_iter % data_freq == 0 && time_iter > 0) {
            saveFields(even);
            tie(start, clock_iter) = restartClock();
        }
        // If this option is chosen in the config file, output some statistics and write the center-line
        // velocity profiles to a text file.
        if (!benchmark && out_freq != 0 && time_iter % out_freq == 0 && time_iter > 0) {
            double maxVel = computeMaxVel(even);
            double permeability = computePermeability(even, buffer, ulb, nu_lb, dx);
            cout << "Permeability: " << setprecision(8) << permeability << endl;
            cout << "Maximum velocity (LU): " << setprecision(8) << maxVel << endl;
            permfile << setw(10) << time_iter * dt << setw(16) << setprecision(8) << permeability << endl;
            if (clock_iter > 0) {
                printMlups(start, clock_iter, dim.nelem);
            }
        }

        // Collision and streaming are fused. Depending on the parity of the current time step,
        // execute one function object or the other one.
        if (time_iter % 2 == 0) {
            for_each(execution::par_unseq, lattice, lattice + dim.nelem, even);
        }
        else {
            for_each(execution::par_unseq, lattice, lattice + dim.nelem, odd);
        }
        ++clock_iter;
    }

    if (benchmark) {
        return printMlups(start, clock_iter, dim.nelem);
    }
    else {
        return 0.;
    }
}

int main()
{
    try {
        set<string> double_param { "nuPhys", "tau", "ulb", "dx" };
        set<string> int_param { "nx", "ny", "nz", "data_nx", "data_ny", "data_nz", "buffer", "out_freq",
                                "vtk_freq", "data_freq", "prod_max_iter", "bench_ini_iter", "bench_max_iter" };
        set<string> bool_param { "benchmark", "unrolled" };
        set<string> string_param { "structure", "rawdata_fname" };
        map<string, std::any> param;
        // Get all parameters from the configuration file (or in some cases, default values).
        param = parse_configfile(double_param, int_param, bool_param, string_param, param);

        out_freq = any_cast<int>(param["out_freq"]);
        vtk_freq = any_cast<int>(param["vtk_freq"]);
        data_freq = any_cast<int>(param["data_freq"]);
        bench_ini_iter = any_cast<int>(param["bench_ini_iter"]);
        bench_max_iter = any_cast<int>(param["bench_max_iter"]);

        nuPhys = any_cast<double>(param["nuPhys"]);
        tau = any_cast<double>(param["tau"]);
        ulb = any_cast<double>(param["ulb"]);
        dx = any_cast<double>(param["dx"]);

        int nx = any_cast<int>(param["nx"]);
        int ny = any_cast<int>(param["ny"]);
        int nz = any_cast<int>(param["nz"]);
        Dim dimExtract(nx, ny, nz);

        int data_nx = any_cast<int>(param["data_nx"]);
        int data_ny = any_cast<int>(param["data_ny"]);
        int data_nz = any_cast<int>(param["data_nz"]);
        Dim dimData(data_nx, data_ny, data_nz);

        int buffer = any_cast<int>(param["buffer"]);
        int prod_max_iter = any_cast<int>(param["prod_max_iter"]);

        bool benchmark = any_cast<bool>(param["benchmark"]);
        bool unrolled = any_cast<bool>(param["unrolled"]);

        string rawdata_fname = any_cast<string>(param["rawdata_fname"]);

        try {
            string structureStr = any_cast<string>(param.at("structure"));
            param["structureStr"] = structureStr;
            param["structure"] = stringToDataStructure().at(structureStr);
        }
        catch(out_of_range const& e) {
            throw invalid_argument("Unknown data structure: " + any_cast<string>(param["structure"]));
        }


        DataStructure data_structure = any_cast<DataStructure>(param["structure"]);
        string data_structure_str = any_cast<string>(param["structureStr"]);
        string model_str = "trt";
        LBModel model { LBModel::trt };

        cout << "LB model: " << model_str << endl;
        cout << "Implementation scheme: " << data_structure_str << endl;
        cout << "Unrolled loops: " << boolalpha << unrolled << endl;

        // Now follows the long list of options to instantiate the right code depending on
        // - Whether to use the two-population scheme, the swap scheme, or the AA-pattern
        // - Which of the nine collision models to use
        // - Whether to use the optimized (unrolled) or educational (not unrolled) code.
        if (data_structure == DataStructure::twopop_aos) {
            if (unrolled) {
                runPorousTwoPop<twopop_aos_trt_unrolled::LBM>(benchmark, prod_max_iter, model, rawdata_fname, dimData, dimExtract, buffer);
            }
            else {
                runPorousTwoPop<twopop_aos::LBM>(benchmark, prod_max_iter, model, rawdata_fname, dimData, dimExtract, buffer);
            }
        }
        else if (data_structure == DataStructure::twopop_soa) {
            if (unrolled) {
                runPorousTwoPop<twopop_soa_trt_unrolled::LBM>(benchmark, prod_max_iter, model, rawdata_fname, dimData, dimExtract, buffer);
            }
            else { // Not unrolled
                runPorousTwoPop<twopop_soa::LBM>(benchmark, prod_max_iter, model, rawdata_fname, dimData, dimExtract, buffer);
            }
        }
        else if (data_structure == DataStructure::swap_aos) {
            if (unrolled) {
                runPorousSwap<swap_aos_trt_unrolled::Collide, swap_aos_trt_unrolled::Stream>(benchmark, prod_max_iter, model, rawdata_fname, dimData, dimExtract, buffer);
            }
            else {
                runPorousSwap<swap_aos::Collide, swap_aos::Stream>(benchmark, prod_max_iter, model, rawdata_fname, dimData, dimExtract, buffer);
            }
        }
        else if (data_structure == DataStructure::swap_soa) {
            if (unrolled) {
                runPorousSwap<swap_soa_trt_unrolled::Collide, swap_soa_trt_unrolled::Stream>(benchmark, prod_max_iter, model, rawdata_fname, dimData, dimExtract, buffer);
            }
            else {
                runPorousSwap<swap_soa::Collide, swap_soa::Stream>(benchmark, prod_max_iter, model, rawdata_fname, dimData, dimExtract, buffer);
            }
        }
        else if (data_structure == DataStructure::aa_aos) {
            if (unrolled) {
                runPorousAA<aa_aos_trt_unrolled::Even, aa_aos_trt_unrolled::Odd>(benchmark, prod_max_iter, model, rawdata_fname, dimData, dimExtract, buffer);
            }
            else {
                runPorousAA<aa_aos::Even, aa_aos::Odd>(benchmark, prod_max_iter, model, rawdata_fname, dimData, dimExtract, buffer);
            }
        }
        else if (data_structure == DataStructure::aa_soa) {
            if (unrolled) {
                runPorousAA<aa_soa_trt_unrolled::Even, aa_soa_trt_unrolled::Odd>(benchmark, prod_max_iter, model, rawdata_fname, dimData, dimExtract, buffer);
            }
            else { // Not unrolled
                runPorousAA<aa_soa::Even, aa_soa::Odd>(benchmark, prod_max_iter, model, rawdata_fname, dimData, dimExtract, buffer);
            }
        }
        else if (data_structure == DataStructure::all) {
            std::vector<double> twopop_aos_mlups, twopop_soa_mlups, swap_aos_mlups, swap_soa_mlups, aa_aos_mlups, aa_soa_mlups;
            benchmark = true; // "all" data structures triggers a special benchmark mode.
            
            if (unrolled) {
                cout << "Running all benchmarks in trt-unrolled mode." << endl;
                for (int i = 0; i < 10; ++i) {
                    cout << "Running twopop_aos attempt " << i << endl;
                    twopop_aos_mlups.push_back(
                        runPorousTwoPop<twopop_aos_trt_unrolled::LBM>(benchmark, prod_max_iter, model, rawdata_fname, dimData, dimExtract, buffer));
                    cout << "Running twopop_soa attempt " << i << endl;
                    twopop_soa_mlups.push_back(
                        runPorousTwoPop<twopop_soa_trt_unrolled::LBM>(benchmark, prod_max_iter, model, rawdata_fname, dimData, dimExtract, buffer));
                    cout << "Running swap_aos attempt " << i << endl;
                    swap_aos_mlups.push_back(
                        runPorousSwap<swap_aos_trt_unrolled::Collide, swap_aos_trt_unrolled::Stream>(benchmark, prod_max_iter, model, rawdata_fname, dimData, dimExtract, buffer));
                    cout << "Running swap_soa attempt " << i << endl;
                    swap_soa_mlups.push_back(
                        runPorousSwap<swap_soa_trt_unrolled::Collide, swap_soa_trt_unrolled::Stream>(benchmark, prod_max_iter, model, rawdata_fname, dimData, dimExtract, buffer));
                    cout << "Running aa_aos attempt " << i << endl;
                    aa_aos_mlups.push_back(
                        runPorousAA<aa_aos_trt_unrolled::Even, aa_aos_trt_unrolled::Odd>(benchmark, prod_max_iter, model, rawdata_fname, dimData, dimExtract, buffer));
                    cout << "Running aa_soa attempt " << i << endl;
                    aa_soa_mlups.push_back(
                        runPorousAA<aa_soa_trt_unrolled::Even, aa_soa_trt_unrolled::Odd>(benchmark, prod_max_iter, model, rawdata_fname, dimData, dimExtract, buffer));
                }
            }
            else {
                cout << "Running all benchmarks in trt mode." << endl;
                for (int i = 0; i < 10; ++i) {
                    cout << "Running twopop_aos attempt " << i << endl;
                    twopop_aos_mlups.push_back(
                        runPorousTwoPop<twopop_aos::LBM>(benchmark, prod_max_iter, model, rawdata_fname, dimData, dimExtract, buffer));
                    cout << "Running twopop_soa attempt " << i << endl;
                    twopop_soa_mlups.push_back(
                        runPorousTwoPop<twopop_soa::LBM>(benchmark, prod_max_iter, model, rawdata_fname, dimData, dimExtract, buffer));
                    cout << "Running swap_aos attempt " << i << endl;
                    swap_aos_mlups.push_back(
                        runPorousSwap<swap_aos::Collide, swap_aos::Stream>(benchmark, prod_max_iter, model, rawdata_fname, dimData, dimExtract, buffer));
                    cout << "Running swap_soa attempt " << i << endl;
                    swap_soa_mlups.push_back(
                        runPorousSwap<swap_soa::Collide, swap_soa::Stream>(benchmark, prod_max_iter, model, rawdata_fname, dimData, dimExtract, buffer));
                    cout << "Running aa_aos attempt " << i << endl;
                    aa_aos_mlups.push_back(
                        runPorousAA<aa_aos::Even, aa_aos::Odd>(benchmark, prod_max_iter, model, rawdata_fname, dimData, dimExtract, buffer));
                    cout << "Running aa_soa attempt " << i << endl;
                    aa_soa_mlups.push_back(
                        runPorousAA<aa_soa::Even, aa_soa::Odd>(benchmark, prod_max_iter, model, rawdata_fname, dimData, dimExtract, buffer));
                }
            }


            // Output the 10 performance measurements for each data structure, and the median value.
            cout << "Summary of performance measurements with Model " << model_str
                 << (unrolled? ", unrolled, " : ", educational, " ) << "ny = " << dimExtract.ny << endl;
            cout << setw(16) << "twopop_aos"
                 << setw(16) << "twopop_soa"
                 << setw(16) << "swap_aos"
                 << setw(16) << "swap_soa"
                 << setw(16) << "aa_aos"
                 << setw(16) << "aa_soa" << endl;
            for (int i = 0; i < 10; ++i) {
                cout << setw(16) << setprecision(5) << twopop_aos_mlups[i]
                     << setw(16) << setprecision(5) << twopop_soa_mlups[i]
                     << setw(16) << setprecision(5) << swap_aos_mlups[i]
                     << setw(16) << setprecision(5) << swap_soa_mlups[i]
                     << setw(16) << setprecision(5) << aa_aos_mlups[i]
                     << setw(16) << setprecision(5) << aa_soa_mlups[i]
                     << setw(16) << setprecision(5) << endl;
            }
            // Compute the median of a vector of 10 elements.
            auto median10 = [](vector<double> v) {
                assert( v.size() == 10 );
                nth_element(v.begin(), v.begin() + 5, v.end()); 
                nth_element(v.begin(), v.begin() + 4, v.end()); 
                return (double)(v[4] + v[5]) / 2.0; 
            };
            cout << "MEDIAN: " << endl;
            cout << setw(16) << setprecision(5) << median10(twopop_aos_mlups)
                 << setw(16) << setprecision(5) << median10(twopop_soa_mlups)
                 << setw(16) << setprecision(5) << median10(swap_aos_mlups)
                 << setw(16) << setprecision(5) << median10(swap_soa_mlups)
                 << setw(16) << setprecision(5) << median10(aa_aos_mlups)
                 << setw(16) << setprecision(5) << median10(aa_soa_mlups)
                 << setw(16) << setprecision(5) << endl;
        }
        else {
            assert( false );
        }
    }
    catch(runtime_error const& e) {
        cout << "Error: Regression test failed." << endl;
        return 2;
    }
    catch(exception const& e) {
        cout << "Error: invalid use of program." << endl;
        cout << e.what() << endl;
        return 1;
    }
    return 0;
}
